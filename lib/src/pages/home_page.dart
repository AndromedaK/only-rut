import 'dart:ui';
import 'package:dart_rut_validator/dart_rut_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';


class HomePage extends StatefulWidget {

  @override
  _HomePageState createState() => _HomePageState();

}

class _HomePageState extends State<HomePage> {

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _rutController = TextEditingController();
  final Color mainColor = Colors.deepPurple;
  final Color secondaryColor = Colors.teal.shade900;

  @override
  void initState() {
    //_rutController = TextEditingController(text: '');
    _rutController.clear();
    super.initState();
  }


  void onChangedApplyFormat(String text) {
    RUTValidator.formatFromTextController(_rutController);
  }



  @override
  Widget build(BuildContext context) {


    return Scaffold(
      appBar: AppBar(
        title: Text("Rut app"),
      ),
      body: Builder(builder: (BuildContext context){
        return ListView(
          children: [
            _buildForm(context)
          ],
        );
      }),
    );

  }

  Widget _buildForm(BuildContext context) {

    return Container(
      margin: EdgeInsets.only(top:100),
      padding: EdgeInsets.symmetric(horizontal: 45),
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            SizedBox(height:150),
            TextFormField(
              maxLines: 1,
              onChanged: onChangedApplyFormat,
              keyboardType: TextInputType.visiblePassword,
              decoration: InputDecoration(
                hintText: 'Ingrese Rut',
                hintStyle: TextStyle(
                    color: secondaryColor.withOpacity(0.6),
                    letterSpacing: 0.6,
                    fontFeatures: [FontFeature.tabularFigures()]),
                icon: Icon( Icons.person, size: 40, color: mainColor.withOpacity(0.6)),
              ),
              controller: _rutController,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.deepPurple[800].withOpacity(0.9)
              ),
            )
          ],
        ) 
      ),
    );
  }
}