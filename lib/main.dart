import 'package:flutter/material.dart';
import 'package:rutvalidador/src/pages/home_page.dart';
 
void main() => runApp(MyApp());
 
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Rut App',
      theme: ThemeData(
          primarySwatch: Colors.teal, //0xFF004D40
          accentColor: Colors.amberAccent[700]),
      home:HomePage()
      
    );
  }
}